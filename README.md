# Project Randomizer

## About
These shell scripts are used to help me pick which project to work on. As someone who struggles to figure out which project to do next, or even remembering projects I've thought of before. I decided to make a schell script to help me pick from a predetermied list. Now, you still have to remember to add projects to your list which is something I've also thought of. So in this project there are two shell scripts. One is used to help you pick a project and the other one is used to add projects to the list. This will make it easier while you're in a terminal to add prjects to your project list and retrieve them. 

## Usage
Once you have cloned this repository you can add the directory path to your $PATH. This will make it easy to run the commands without being in this directory. Simpily type ```add_new_project <project_name>``` to add a project to the list. Or, ```get_new_project``` to get a new project. This will pick a random item from your list. You may have to run ```chmod +x <file_name>``` if you get a permission error.

## TODO 
* Add categories for different project lists
* Work in all shells (this is BASH specific)
* Use a better randomizer
* Should probably change to use something like Python for better performance and storage of tasks
